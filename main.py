from game import Game
# from player import Player

"""Plateformer."""
"""
main.py - Code de base pour lancer l'UI
constants.py - Fichier de constantes
player.py - Classes pour le joueur
img/*.png - graphics assets
"""

# create the game object
g = Game()
g.show_start_screen()
while True:
    g.new()
    g.run()
    g.show_go_screen()
