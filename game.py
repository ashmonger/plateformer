#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import os
import sys
import pygame as pg
from player import Player
from plateforms import Plateforms
from settings import *


class Game:

    """The base game itself."""

    def __init__(self):
        """Init of the game class."""
        pg.init()
        pg.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=1024)
        pg.mixer.init()
        # pg.mixer.music.play()
        # pg.mixer.music.set_volume(0.5)
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        self.clock.tick(FPS)
        pg.key.set_repeat(200, 100)
        # self.load_data()

    def quit(self):
        """To quit the game."""
        pg.quit()
        sys.exit()

    def new(self):
        """Initialize all variables and setup for a new game."""
        self.all_sprites = pg.sprite.Group()
        self.walls = pg.sprite.Group()
        self.player = Player(self)
        self.plateforms = pg.sprite.Group()
        for ptlf in PLATEFORM_LIST:
            p = Plateforms(*ptlf)
            self.plateforms.add(p)
            self.all_sprites.add(p)
        self.all_sprites.add(self.player)

    def run(self):
        """Game loop - set self.playing = False to end the game."""
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000
            self.events()
            self.update()
            self.draw()

    def update(self):
        """"Update portion of the game loop."""
        self.all_sprites.update()
        # check if player hits ptlf - only if falling
        if self.player.vel.y != 0 or self.player.vel.x != 0:
            hits = pg.sprite.spritecollide(self.player, self.plateforms, False)
            if hits:
                # self.player.pos.y = hits[0].rect.top + 1
                # self.player.pos.y = hits[0].rect.bottom - 1
                self.player.vel.x = 0
                self.player.vel.y = 0

    def draw_grid(self):
        """To draw the grid, dev purpose only."""
        for x in range(0, WIDTH, TILESIZE):
            pg.draw.line(self.screen, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pg.draw.line(self.screen, LIGHTGREY, (0, y), (WIDTH, y))

    def draw(self):
        """Draw everything."""
        self.screen.fill(BGCOLOR)
        self.draw_grid()
        self.all_sprites.draw(self.screen)
        pg.display.flip()

    def events(self):
        """Catch all events here."""
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.quit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    self.quit()
                if event.key == pg.K_SPACE:
                    self.player.jump()

    def show_start_screen(self):
        """Start screen."""
        pass

    def show_go_screen(self):
        """Pre-game screen."""
        pass
