#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
main.py - Code de base pour lancer l'UI
lab.py - Classes pour la création du Labyrinthe
player.py - Classes pour le joueur
constants.py - Fichier de constantes
img/*.png - cases du Labyrinthe
"""

import pygame as pg
from settings import *

vec = pg.math.Vector2


class Player(pg.sprite.Sprite):
    def __init__(self, game):
        pg.sprite.Sprite.__init__(self)
        self.game = game
        # self.image = pg.Surface((32, 32))
        # self.image.fill(GREEN)
        player_img = pg.image.load(
            '{}/assets/img/player.png'.format(BASEDIR)
        ).convert_alpha()
        self.image = player_img
        self.rect = self.image.get_rect()
        self.pos = vec(WIDTH / 2, HEIGHT / 2)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)

    def jump(self):
        pass
        # self.rect.y += 1
        # hits = pg.sprite.spritecollide(self, self.game.plateforms, False)
        # self.rect.y += 1
        # if hits:
        #     self.vel.y = -20

    def update(self):
        self.acc = vec(0, 0)
        keys = pg.key.get_pressed()
        if keys[pg.K_LEFT]:
            self.acc.x = -PLAYER_ACCEL
        if keys[pg.K_RIGHT]:
            self.acc.x = PLAYER_ACCEL
        if keys[pg.K_UP]:
            self.acc.y = -PLAYER_ACCEL
        if keys[pg.K_DOWN]:
            self.acc.y = PLAYER_ACCEL

        # Apply friction
        self.acc.x += self.vel.x * PLAYER_FRICT
        self.acc.y += self.vel.y * PLAYER_FRICT
        self.vel += self.acc

        # normalize diagonals
        if self.vel.x != 0 and self.vel.y != 0:
            self.vel.normalize()
        # equiation of mouvement
        self.pos += self.vel + 0.5 * self.acc

        self.rect.midbottom = self.pos

        if self.pos.x >= (WIDTH - 16):
            self.pos.x = WIDTH - 16
        if self.pos.x <= 16:
            self.pos.x = 16
        if self.pos.y <= 32:
            self.pos.y = 32
        if self.pos.y >= HEIGHT:
            self.pos.y = HEIGHT
