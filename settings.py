#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Plateformer
main.py - Code de base pour lancer l'UI
settings.py - Paramètres
"""
import sys

# Basedir change if game is frozen
if getattr(sys, 'frozen', False):
    BASEDIR = sys._MEIPASS
else:
    BASEDIR = "."
# BASEDIR = os.path.dirname(__file__)

# Colors
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
TEAL = (0, 255, 255)
PURPLE = (255, 0, 255)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
DARKGREY = (64, 64, 64)
LIGHTGREY = (128, 128, 128)

# Game settings
WIDTH = 1024
HEIGHT = 768
TILESIZE = 32
GRIDWIDTH = WIDTH / TILESIZE
GRIDHEIGHT = HEIGHT / TILESIZE

TITLE = "Plateformer"
BGCOLOR = DARKGREY
FPS = 60
GRAVITY = 0.8

# Player Properties
PLAYER_ACCEL = 0.5
PLAYER_FRICT = -0.1

# Plateforms List (x, y, w, h)
PLATEFORM_LIST = [
    # (0, HEIGHT - TILESIZE, WIDTH, TILESIZE),  # Floor
    (5 * TILESIZE, 17 * TILESIZE, 5 * TILESIZE, TILESIZE),
    (20 * TILESIZE, 14 * TILESIZE, 3 * TILESIZE, TILESIZE),
    (10 * TILESIZE, 5 * TILESIZE, 4 * TILESIZE, TILESIZE),
    (13 * TILESIZE, 12 * TILESIZE, 6 * TILESIZE, TILESIZE),
    (25 * TILESIZE, 19 * TILESIZE, 7 * TILESIZE, TILESIZE)
]
